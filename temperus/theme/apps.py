from django.apps import AppConfig


class ThemeConfig(AppConfig):
    name = "temperus.theme"
