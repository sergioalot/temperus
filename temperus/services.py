import datetime as dt
import json
import os
from collections import defaultdict
from typing import List

import attrs
import requests
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

URL_API_WEATHER = "https://api.openweathermap.org/data/2.5/forecast"


def obtain_icon_url(icon_name: str) -> str:
    # Obtain url from icon name api openweathermap
    # https://openweathermap.org/weather-conditions

    icons = defaultdict(lambda: "mist")  # default value is mist
    icons.update(
        {
            "01d": "sun",
            "01n": "moon",
            "02d": "cloud-sun",
            "02n": "cloud-moon",
            "03d": "cloud",
            "03n": "cloud",
            "04d": "cloud",
            "04n": "cloud",
            "09d": "shower-rain",
            "09n": "shower-rain",
            "10d": "rain-sun",
            "10n": "rain-moon",
            "11d": "thunderstorm",
            "11n": "thunderstorm",
            "13d": "snow",
            "13n": "snow",
            "50d": "mist",
            "50n": "mist",
        }
    )

    # @TODO Return static url
    return f"img/weather/{icons[icon_name]}.svg"


def string_to_datetime(string: str) -> dt.datetime:
    return dt.datetime.strptime(string, "%Y-%m-%d %H:%M:%S")


def obtainWeatherApi(ubication: str) -> dict:  # pragma: no cover
    app_id = os.environ.get("API_KEY", default="api_key")

    r = requests.get(
        f"{ URL_API_WEATHER }?appid={app_id}&q={ubication}&units=metric&lang=es"
    )

    return json.loads(r.text)


@attrs.define
class WeatherDay:  # pragma: no cover
    datetime: str
    temp: float
    feels_like: float
    temp_min: float
    temp_max: float
    weather: str
    weather_icon_url: str
    humidity: int
    wind: float

    @property
    def feels_like_label(self) -> str:
        return f"{self.feels_like}°"

    @property
    def humidity_label(self) -> str:
        return f"{self.humidity} %"

    @property
    def wind_label(self) -> str:
        return f"{self.wind} km/h"


@attrs.define
class WeatherData:
    city: str
    country: str
    current_weather: WeatherDay
    today: List[WeatherDay] = []
    future_days: List[WeatherDay] = []


def obtainWeatherData(ubication: str) -> dict:  # pragma: no cover
    weather = obtainWeatherApi(ubication)

    if weather["cod"] != "200":
        return {"errors": _("There were some problems. Refresh the page")}

    if weather["city"]["name"] is None:
        return {"errors": _("The city does not exist")}

    current_weather = WeatherDay(
        string_to_datetime(weather["list"][0]["dt_txt"]),
        weather["list"][0]["main"]["temp"],
        weather["list"][0]["main"]["feels_like"],
        weather["list"][0]["main"]["temp_min"],
        weather["list"][0]["main"]["temp_max"],
        weather["list"][0]["weather"][0]["description"],
        obtain_icon_url(weather["list"][0]["weather"][0].get("icon")),
        weather["list"][0]["main"]["humidity"],
        weather["list"][0]["wind"]["speed"],
    )

    today_weather_list = [
        WeatherDay(
            string_to_datetime(day["dt_txt"]),
            day["main"]["temp"],
            day["main"]["feels_like"],
            day["main"]["temp_min"],
            day["main"]["temp_max"],
            day["weather"][0]["description"],
            obtain_icon_url(day["weather"][0].get("icon")),
            day["main"]["humidity"],
            day["wind"]["speed"],
        )
        for day in weather["list"]
        if obtain_current_day() == string_to_datetime(day["dt_txt"]).date()
    ]

    future_days_weather = [
        WeatherDay(
            string_to_datetime(day["dt_txt"]),
            day["main"]["temp"],
            day["main"]["feels_like"],
            day["main"]["temp_min"],
            day["main"]["temp_max"],
            day["weather"][0]["description"],
            obtain_icon_url(day["weather"][0].get("icon")),
            day["main"]["humidity"],
            day["wind"]["speed"],
        )
        for day in weather["list"]
        if obtain_current_day() != string_to_datetime(day["dt_txt"]).date()
    ]

    future_days_weather = [day for day in future_days_weather if day.datetime.hour == 0]

    return WeatherData(
        weather["city"]["name"],
        weather["city"]["country"],
        current_weather,
        today_weather_list,
        future_days_weather,
    )


# Timezone activate según la zona horaria del usuario
def obtain_current_day() -> dt.date:
    timezone.activate(timezone.get_current_timezone())
    return timezone.localtime().date()
