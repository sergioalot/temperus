from django.http import Http404
from django.shortcuts import resolve_url
from django.utils.translation import gettext_lazy as _
from django.views.generic import FormView, TemplateView

import temperus.webapp.forms as webapp_forms
from temperus import services
from temperus.webapp.views.mixins import PartialRenderingMixin


class ObtainWeatherDataView(PartialRenderingMixin, FormView):
    folder_template = "obtain_weather_data"
    form_class = webapp_forms.WeatherForm

    def form_valid(self, form):
        self.city = form.obtain_city()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context_data = super().get_context_data(**kwargs)
        return context_data

    def get_success_url(self) -> str:
        return resolve_url("weather_data", self.city)


class WeatherDataView(PartialRenderingMixin, TemplateView):
    folder_template = "weather_data"

    def get(self, request, *args, **kwargs):
        self.city = self.get_city()
        return super().get(request, args, kwargs)

    def get_city(self, queryset=None):
        city_name = self.kwargs.get("city")

        if city_name is None:  # pragma: no cover
            raise Http404(_("No city matches the given query."))

        return city_name

    def get_context_data(self, **kwargs):
        context_data = super().get_context_data(**kwargs)
        context_data["weather_data"] = services.obtainWeatherData(self.city)

        return context_data
