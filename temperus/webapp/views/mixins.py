from django.http import Http404


class PartialRenderingMixin:
    full_mode: bool = True
    folder_template: str = ""

    def setup(self, request, *args, **kwargs):
        super().setup(request, *args, **kwargs)

        if self.full_mode is False and not bool(self.request.htmx):
            raise Http404

    def get_template_names(self):
        template_name = "partial" if self.request.htmx else "full"

        return [f"{self.folder_template}/{template_name}.html"]
