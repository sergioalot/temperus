from django.urls import path

from temperus.webapp.views import weather

urlpatterns = [
    path("", weather.ObtainWeatherDataView.as_view(), name="obtain_weather_data"),
    path("weather/<str:city>/", weather.WeatherDataView.as_view(), name="weather_data"),
]
