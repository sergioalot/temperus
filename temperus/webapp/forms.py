from django import forms
from django.utils.translation import gettext as _


class WeatherForm(forms.Form):
    city = forms.CharField(
        label=_("City"), help_text=_("The weather in..."), max_length=100
    )

    class Meta:
        fields = ["city"]

    def obtain_city(self):
        return self.cleaned_data.get("city")
