from temperus.webapp.views import weather


class TestObtainWeatherDataView:
    def test_get_method(self, rf):
        # Arrange
        view = weather.ObtainWeatherDataView

        request = rf.get("/")
        request.htmx = True

        # Act
        response = view.as_view()(request)

        # Assert
        assert response.status_code == 200
        assert response.render()

    def test_post_method(self, rf):
        # Arrange
        view = weather.ObtainWeatherDataView

        request = rf.post("/", {"city": "London"})
        request.htmx = True

        # Act
        response = view.as_view()(request)

        # Assert
        assert response.status_code == 302


class TestWeatherDataView:
    def test_get_method(self, rf):
        # Arrange
        view = weather.WeatherDataView

        request = rf.get("/")
        request.htmx = True

        # Act
        response = view.as_view()(request, city="London")

        # Assert
        assert response.status_code == 200
        assert response.render()
