from temperus.webapp.forms import WeatherForm


def test_weather_form_is_valid():
    form = WeatherForm({"city": "London"})
    assert form.is_valid()
    assert form.obtain_city() == "London"
