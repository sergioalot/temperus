import pytest
from django.http import Http404
from django.views.generic import TemplateView

from temperus.webapp.views.mixins import PartialRenderingMixin


class TestPartialRenderingMixin:
    def test_not_full_mode_without_htmx(self, rf):
        # Arrange
        class TestView(PartialRenderingMixin, TemplateView):
            full_mode = False
            folder_template = "test"

        request = rf.get("/")
        request.htmx = False

        # Act
        view = TestView.as_view()

        # Assert
        with pytest.raises(Http404):
            view(request)

    def test_htmx(self, subtests, rf):
        # Arrange
        parametrize = [
            # htmx, template_name
            (False, "full"),
            (True, "partial"),
        ]

        class TestView(PartialRenderingMixin, TemplateView):
            folder_template = "test"

        for htmx, template_name in parametrize:
            with subtests.test(htmx=htmx, template_name=template_name):
                request = rf.get("/")
                request.htmx = htmx

                # Act
                response = TestView.as_view()(request)

                # Assert
                assert response.template_name == [f"test/{template_name}.html"]
