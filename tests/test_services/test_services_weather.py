import datetime as dt

import temperus.services


def test_obtain_icon_url():
    assert temperus.services.obtain_icon_url("01d") == "img/weather/sun.svg"


def test_obtain_icon_url_not_exists():
    assert temperus.services.obtain_icon_url("not_exist") == "img/weather/mist.svg"


def test_string_to_datetime():
    assert temperus.services.string_to_datetime(
        "2020-01-01 01:01:01"
    ) == dt.datetime.strptime("2020-01-01 01:01:01", "%Y-%m-%d %H:%M:%S")


def test_obtain_current_day():
    assert temperus.services.obtain_current_day() == dt.date.today()
