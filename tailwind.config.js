module.exports = {
  content: [
       './temperus/theme/templates/**/*.html',
       './temperus/theme/templates/*.html',
       './temperus/webapp/templates/**/*.html',
       './temperus/webapp/templates/*.html',
  ],
  theme: {
    extend: {},
  },
  plugins: [],
}
